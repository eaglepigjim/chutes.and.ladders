﻿using System;
using System.Collections.Generic;

namespace ChutesAndLadders
{
    //Board class to hold the details of what each square does
    internal class Board
    {
        //Total number of squares in the game, also lets us know the winning square to get to
        internal int NumberOfSquares { get; set; } = 100;

        //At the moment we don't need 2 separate collections of Ladders and Chutes, but we could easily change
        //that here by adding a second Dictionary.
        internal Dictionary<int, int> SpecialSquares { get; set; }

        internal Board()
        {
            //Initialize the Ladders and Chutes using object initializer
            SpecialSquares = new Dictionary<int, int>
            {
                {10, 18},
                {20, 14},
                {24, 35},
                {30, 40},
                {32, 15},
                {41, 57},
                {45, 55},
                {48, 60},
                {50, 25},
                {51, 64},
                {61, 43},
                {63, 70},
                {78, 65},
                {80, 100},
                //{48, 53} This is invalid, can't have a snake and ladder at same spot
            };
        }

        //Method to calculate if the player has hit a chute or a ladder
        internal int CalculateNewPosition(int potentialNewSquare)
        {
            //See if we have already reached the end of the game, if so return the last square
            if (potentialNewSquare >= NumberOfSquares)
                return NumberOfSquares;

            //Check Dictionary to see if any of our keys match the square just moved to
            if (!SpecialSquares.ContainsKey(potentialNewSquare))
                return potentialNewSquare;  //No match so just return the square

            //We hit a special square so lets get the destination value
            var destination = SpecialSquares[potentialNewSquare];

            //Tell the user if the went up a ladder or down a chute
            Console.WriteLine(destination > potentialNewSquare
                ? $"You took a ladder to {destination}!"
                : $"You took a chute to {destination}!");

            //Return the new destination
            return destination;
        }
    }
}
