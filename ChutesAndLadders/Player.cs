﻿namespace ChutesAndLadders
{
    //Player object can be very simple as it just needs to hold a Number and current position at the moment.
    //Easily extensible later if we need name or color etc.
    internal class Player
    {
        //Number for the player currently used as the name
        internal int Number { get; set; }

        //Current position where they are on the board
        internal int Position { get; set; }
    }
}
