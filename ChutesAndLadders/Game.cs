﻿using System;
using System.Collections.Generic;

namespace ChutesAndLadders
{
    internal class Game
    {
        //Create configurable settings for future modifications
        private int MinimumNumberOfPlayers { get; } = 2;
        private int MaximumNumberOfPlayers { get; } = 4;

        //Players are in order so a FIFO collection would work best here
        internal Queue<Player> Players { get; set; }

        internal Game()
        {
            Players = new Queue<Player>();
        }

        //Create method to ensure valid input for number of players
        internal int GetNumberOfPlayers()
        {
            bool success;
            int playerCount;

            do
            {
                var numberOfPlayers = Console.ReadLine();

                //Ensure a number is entered
                success = int.TryParse(numberOfPlayers, out playerCount);

                if (!success)
                    Console.WriteLine($"Please enter a number between {MinimumNumberOfPlayers} and " +
                                      $"{MaximumNumberOfPlayers}");
                else
                {
                    //Make sure the number of players is within game rules
                    if (playerCount >= MinimumNumberOfPlayers && playerCount <= MaximumNumberOfPlayers)
                        continue;

                    Console.WriteLine("Invalid number of players, please select a number between " +
                                      $"{MinimumNumberOfPlayers} and {MaximumNumberOfPlayers}");
                    success = false;
                }
            } while (!success);

            return playerCount;
        }

        //Method to create players in case we want more complex Player objects in future such as names, color of
        //counters etc.
        internal void CreatePlayers(int numberOfPlayers)
        {
            for (var i = 1; i <= numberOfPlayers; i++)
            {
                //Add each player and set their start position
                Players.Enqueue(new Player { Number = i, Position = 1 });
            }
        }

        internal void PlayGame()
        {
            //Set up initial variables
            var dice = new Dice();
            var board = new Board();
            var winner = false;

            //Uncomment to run speed tests
            //var start = DateTime.Now;

            //Simple Boolean do/while loop instead of checking the players collection on each game loop
            do
            {
                //Grab the next player from the queue
                var currentPlayer = Players.Dequeue();

                //Get the number of moves and tell user
                var move = dice.Roll();
                Console.WriteLine($"Player {currentPlayer.Number} spun a {move}.");

                //Tell the player where we are moving to
                var potentialNewSquare = currentPlayer.Position + move;

                //If it is greater than the number of squares, set to number of squares since you can't move past that
                Console.WriteLine($"Player {currentPlayer.Number} moved from {currentPlayer.Position} to square " +
                    $"{(potentialNewSquare > board.NumberOfSquares ? board.NumberOfSquares : potentialNewSquare)}.");

                var validatedNewSquare = board.CalculateNewPosition(potentialNewSquare);

                //If the new square is the winning square then close the game
                if (validatedNewSquare == board.NumberOfSquares)
                {
                    winner = true;
                    Console.WriteLine($"Player {currentPlayer.Number} wins the game!");
                }
                else
                {
                    //Check for any chutes or ladders
                    currentPlayer.Position = validatedNewSquare;

                    //Put the current player back at the end of the queue
                    Players.Enqueue(currentPlayer);
                }
            } while (!winner);

            //Uncomment to run speed tests
            //Console.WriteLine($"The game took {DateTime.Now - start}");
        }

        //Create method to ensure valid input for play again
        internal bool PlayAgain()
        {
            Console.WriteLine("Would you like to play again? Y/N");

            var validResponse = false;

            do
            {
                var playAgain = Console.ReadLine();

                if (playAgain == null)
                    continue;

                if (playAgain.ToUpper().StartsWith("Y"))
                {
                    Console.Clear();
                    validResponse = true;
                }
                else if (playAgain.ToUpper().StartsWith("N"))
                    return false;
                else
                    Console.WriteLine("Please enter either Y or N");
            } while (!validResponse);

            return true;
        }
    }
}