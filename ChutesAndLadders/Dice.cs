﻿using System;

namespace ChutesAndLadders
{
    //Create a separate class to manage the random number rolling
    internal class Dice
    {
        //Create a single instance of the random to avoid issues with reusing Random
        private static readonly Random Random = new Random();

        //Create configurable min max rolls for players
        internal int NumberOfDice { get; set; } = 1;
        internal int NumberOfSides { get; set; } = 6;

        //Roll dice method for number of dice and sides
        internal int Roll()
        {
            var totalRoll = 0;

            for (var i = 0; i < NumberOfDice; i++)
                totalRoll += Random.Next(0, NumberOfSides) + 1;

            return totalRoll;
        }
    }
}