﻿using System;

namespace ChutesAndLadders
{
    internal class Program
    {
        private static void Main()
        {
            while (true)
            {
                //Create a new game
                var game = new Game();

                Console.WriteLine("Welcome to chutes and ladders! How many players?");

                //Get and create the players based on valid input
                var numberOfPlayers = game.GetNumberOfPlayers();
                game.CreatePlayers(numberOfPlayers);

                //If we wanted a more configurable game we could also make our Board and Dice configurable here

                //Play the game
                game.PlayGame();

                //Ensure we get a valid response to see if we want to play again
                if (!game.PlayAgain())
                    return;
            }
        }
    }
}